package com.arsenic.covidapi.controllers;

import com.arsenic.covidapi.helpers.CovidDataScraper;
import com.arsenic.covidapi.helpers.GetAllData;
import com.arsenic.covidapi.restservice.Covid;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
public class CovidController {
    @RequestMapping("/covid")
    public Covid covid(@RequestParam("country") String countryName) {
        CovidDataScraper scraper = new CovidDataScraper();
        GetAllData data = scraper.getData(countryName);

        List<String> updates = new ArrayList<>();

        Pattern pattern = Pattern.compile("\\b\\d[\\d,.]*\\b");
        Matcher updatesMatch = pattern.matcher(data.updates); //

        while(updatesMatch.find()) {
            updates.add(updatesMatch.group());
        }

        return new Covid(countryName, data.totalData, updates);
    }
}
