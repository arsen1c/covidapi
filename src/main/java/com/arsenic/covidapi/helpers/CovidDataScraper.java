package com.arsenic.covidapi.helpers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class CovidDataScraper {
    public GetAllData getData(String countryName) {
        String updates = null;
        String[] totalCases = {};
        String[] splitName = countryName.split(" ");
        countryName = String.join("-", splitName);

        try {
            Document doc = Jsoup.connect("https://www.worldometers.info/coronavirus/country/" + countryName).get();

            updates = doc.select("li.news_li").first().text();
            // Return String[]: {totalCases, totalDeaths, totalRecovered}
            totalCases = doc.select(".maincounter-number").text().split(" ");
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Return a object
        return new GetAllData(updates, totalCases);
    }
}
