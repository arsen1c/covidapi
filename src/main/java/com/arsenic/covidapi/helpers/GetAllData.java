package com.arsenic.covidapi.helpers;

public class GetAllData {
    public String updates;
    public String[] totalData;

    GetAllData(String updates, String[] totalData) {
        this.updates = updates;
        this.totalData = totalData;
    }
}
