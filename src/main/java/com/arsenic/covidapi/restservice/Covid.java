package com.arsenic.covidapi.restservice;

import java.util.List;

public class Covid {
    private final String countryName;
    private final String[] totalData;
    private final List<String> updates;

    public Covid(String countryName, String[] totalData, List<String> updates) {
        // This is how the JSON response will look like
        this.countryName = countryName;
        this.totalData = totalData;
        this.updates = updates;
    }

    public String getCountryName() {
        return countryName;
    }

    public String[] getTotalData() {
        return totalData;
    }

    public List<String> getUpdates() {
        return updates;
    }
}
